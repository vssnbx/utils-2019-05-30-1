package com.pom_utils.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.cookie.CookieSpec;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicHeader;

public class GitlabHttpIso {
	
	private static final String GITLAB_HOST = "gitlab.intranet";
//	private static final String GITLAB_HOST = "localhost:10000";
	
	private CloseableHttpClient closeableHttpClient; 	
	private BasicCookieStore basicCookieStore;
	private String authenticityToken; 
	
	private static GitlabHttpIso instance;
	
	private GitlabHttpIso() {
	}

	public static synchronized GitlabHttpIso getInstance() {
		if (null == instance) {
			instance = new GitlabHttpIso();
		}
		
		return instance;
	}
	
	public void begin() {
		closeableHttpClient = getHttpClient();
		basicCookieStore = new BasicCookieStore();		
	}

	private CloseableHttpClient getHttpClient() {
		CookieHandler.setDefault(new CookieManager());
		
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		
		httpClientBuilder.setDefaultCookieStore(getBasicCookieStore());
		
		httpClientBuilder.setRedirectStrategy(new LaxRedirectStrategy());
		
//		RequestConfig requestConfig = RequestConfig.DEFAULT;
//		httpClientBuilder.setDefaultRequestConfig(requestConfig);
		httpClientBuilder.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build());
		
		List<Header> defaultHeaders = new ArrayList<Header>();
		defaultHeaders.add(new BasicHeader("Host",                      "gitlab.intranet"));
		defaultHeaders.add(new BasicHeader("Connection",                "keep-alive"));
		defaultHeaders.add(new BasicHeader("Upgrade-Insecure-Requests", "1"));
		defaultHeaders.add(new BasicHeader("User-Agent",                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36"));
		defaultHeaders.add(new BasicHeader("Accept",                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"));
		defaultHeaders.add(new BasicHeader("Accept-Language",           "en-US,en;q=0.9,pt-BR;q=0.8,pt;q=0.7"));
		
		httpClientBuilder.setDefaultHeaders(defaultHeaders);
		CloseableHttpClient closeableHttpClient = httpClientBuilder.build();
		return closeableHttpClient;
	}
	
	public void getLogin() throws Exception {
		logNewExecution("getLogin()");		
		
		HttpGet httpGet = new HttpGet("http://"+getGitlabHost()+"/users/sign_in");
		CloseableHttpResponse closeableHttpResponse = execute(closeableHttpClient, httpGet);
		String content = getContent(closeableHttpResponse);
		
		setAuthenticityToken(getAuthenticityToken(content));
		
		close(closeableHttpResponse);
	}

	public void postLogin() throws Exception {
		logNewExecution("postLogin()");
		
		HttpPost httpPost = new HttpPost("http://"+getGitlabHost()+"/users/auth/ldapmain/callback");
		
//		httpPost.setHeader(new BasicHeader("Referer",      "http://"+getGitlabHost()+"/users/sign_in"));
		httpPost.setHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));

		//URLEncoder.encode(getAuthenticityToken(), "UTF-8"))		
		URLCodec urlCodec = new URLCodec(); //urlCodec.decode("%E2%9C%93", "UTF-8"); urlCodec.encode("", "UTF-8");
		List<NameValuePair> entityRequest = new ArrayList<>(); 
		
//		entityRequest.add(new BasicHeader("utf8",               URLDecoder.decode("%E2%9C%93", "UTF-8")));
//		entityRequest.add(new BasicHeader("authenticity_token", getAuthenticityToken()));
//		entityRequest.add(new BasicHeader("username",           "vagner.dos.s.silva"));
//		entityRequest.add(new BasicHeader("password",           "vivere*123")); 
//		entityRequest.add(new BasicHeader("remember_me",        "1"));
		
		entityRequest.add(new BasicHeader("utf8",               "%E2%9C%93"));
		entityRequest.add(new BasicHeader("authenticity_token", URLEncoder.encode(getAuthenticityToken() , "UTF-8")));
		entityRequest.add(new BasicHeader("username",           URLEncoder.encode("vagner.dos.s.silva"   , "UTF-8")));
		entityRequest.add(new BasicHeader("password",           URLEncoder.encode("vivere*123"           , "UTF-8"))); 
		entityRequest.add(new BasicHeader("remember_me",        URLEncoder.encode("1"                    , "UTF-8")));

		httpPost.setEntity(new UrlEncodedFormEntity(entityRequest, "UTF-8"));
		
		CloseableHttpResponse closeableHttpResponse1 = execute(closeableHttpClient, httpPost);
		String content1 = getContent(closeableHttpResponse1);
		close(closeableHttpResponse1);
	}
	
	public String getPage(Integer page) throws Exception {
		logNewExecution("getPage()");
		
		HttpGet httpGet = new HttpGet("http://"+getGitlabHost()+"/?non_archived=true&page="+page+"&sort=latest_activity_desc");
		CloseableHttpResponse closeableHttpResponse = execute(closeableHttpClient, httpGet);
		String content = getContent(closeableHttpResponse);
		close(closeableHttpResponse);
		
		return content;
	}
	
	private CloseableHttpResponse execute(CloseableHttpClient closeableHttpClient, HttpUriRequest request) throws ClientProtocolException, IOException {
		CloseableHttpResponse closeableHttpResponse = closeableHttpClient.execute(request);
		logHttpResponse(closeableHttpResponse);
		
		return closeableHttpResponse;
	}

	private String getContent(CloseableHttpResponse closeableHttpResponse) throws IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(closeableHttpResponse.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		String content = result.toString();
		System.out.println(content);
		
		return content;
	}
	
	private String getAuthenticityToken(String content) {
		// <input type="hidden" name="authenticity_token"	value="lvivGCpF7qtDbF4J2B4FvOLCuja48gBejInU6qao7uniuOV8xtUgxUEcAveQ+NsMkgWQliuFCWAQQaUPi2h0iQ==" />		
		//htmltag = Pattern.compile("<a\\b[^>]*href=\"[^>]*>(.*?)</a>");
		//link = Pattern.compile("href=\"[^>]*\">");	
		
//		Pattern patternInput = Pattern.compile("(<input[^>]*name=\"authenticity_token\"[^>]*/>)", Pattern.CASE_INSENSITIVE & Pattern.COMMENTS & Pattern.DOTALL & Pattern.MULTILINE & Pattern.UNICODE_CASE);
//		Pattern patternInput = Pattern.compile("<input([^>]*)name=\"authenticity_token\"([^>]*)/>");
//		Pattern patternInput = Pattern.compile("(<input[^>]*name=\"authenticity_token\"[^>]*/>)");
//		Pattern patternInput = Pattern.compile("(<meta[^>]*name=\"authenticity_token\"[^>]*/>)");		
//		
//		Matcher matcherInput = patternInput.matcher(content);
//		matcherInput.find();
//		String groupInput = matcherInput.group(1); //matcherInput.groupCount() matcherInput.group(1)
//		
//		Pattern patternValue = Pattern.compile("value=\"([^\"]*)\"");
//		Matcher matcherValue = patternValue.matcher(groupInput);
//		matcherValue.find();
//		String groupValue = matcherValue.group(1); //matcherValue.groupCount() matcherValue.group(1)
//		System.out.println(groupValue);
		
		// ===================
		
//		<meta name="csrf-token" content="TwabGn0Dg11DnzzUcU5XISLic8Im48al5C1E19nnEVtnHYa+8Pf3iM2IyzPLzNNZ/3AzB7vwP6pXxAkIQGGedw==" />		
		Pattern patternInput;
		Matcher matcherInput;
//		Pattern patternValue;
//		Matcher matcherValue;
		String groupValue;
		
		patternInput = Pattern.compile("<meta name=\"csrf-token\" content=\"([^\"]*)\" />");
		matcherInput = patternInput.matcher(content);
		matcherInput.find();
		groupValue = matcherInput.group(1); // matcherInput.groupCount() matcherInput.group(1)
		System.out.println(groupValue);
		
		return groupValue;
	}
	
	private void close(CloseableHttpResponse closeableHttpResponse) {
		try {
			closeableHttpResponse.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void end() {
		try {
			closeableHttpClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	private void logNewExecution(String desc) {
		System.out.println("###########################################################################################");  
		System.out.println("["+desc+"]");
	}
	
	private void logHttpResponse(CloseableHttpResponse closeableHttpResponse) throws IOException {
		System.out.println("");
		System.out.println("# INI - logHttpResponse");
		
		System.out.println("Set-Cookie");
		Optional<Header> lastCookie = getCookie(closeableHttpResponse);
		System.out.printf("name=[%1$s] value=[%2$s]\n", 
				lastCookie.isPresent() ? lastCookie.get().getName() : "null", 
				lastCookie.isPresent() ? lastCookie.get().getValue() : "null");

		System.out.println("StatusCode");
		StatusLine statusLine = closeableHttpResponse.getStatusLine();
		System.out.printf("statusLine.getStatusCode()=[%1$s]\n", statusLine.getStatusCode());
		
		System.out.println("Headers");
		Arrays.asList(closeableHttpResponse.getAllHeaders()).stream()
			.forEach(header -> System.out.printf("name=[%1$s] value=[%2$s]\n", header.getName(), header.getValue()));
		/*
		System.out.println("Content");
		String content = getContent(closeableHttpResponse);
		*/
		
		System.out.println("# FIM - logHttpResponse");
		System.out.println("");
	}

	@Deprecated
	private String getGitLabSession(Optional<Header> cookie) {
		String result = "";
		String[] split = cookie.get().getValue().split(";");
		for (String string : split) {
			if (StringUtils.containsIgnoreCase(string, "gitlab_session")) {
				return string;
			}
		}
		return result;
	}

	@Deprecated
	private Optional<Header> getCookie(CloseableHttpResponse closeableHttpResponse) {
		Optional<Header> lastCookie = Optional.ofNullable(closeableHttpResponse.getFirstHeader("Set-Cookie"));
		return lastCookie;
	}
	
	private static String getGitlabHost() {
		return GITLAB_HOST;
	}

	private BasicCookieStore getBasicCookieStore() {
		return basicCookieStore;
	}

	private String getAuthenticityToken() {
		return authenticityToken;
	}

	private void setAuthenticityToken(String authenticityToken) {
		this.authenticityToken = authenticityToken;
	}
	
}
