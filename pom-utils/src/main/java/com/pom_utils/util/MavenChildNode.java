package com.pom_utils.util;

import org.w3c.dom.Node;

public class MavenChildNode {
	
	private Node groupId = null;
	private Node artifactId = null;
	private Node version = null;
	
	public MavenChildNode(Node childItemGroupId, Node childItemArtifactId, Node childItemVersion) {
		this.groupId = childItemGroupId;
		this.artifactId = childItemArtifactId;
		this.version = childItemVersion;
	}

	public Node getGroupId() {
		return groupId;
	}

	public void setGroupId(Node groupId) {
		this.groupId = groupId;
	}

	public Node getArtifactId() {
		return artifactId;
	}

	public void setArtifactId(Node artifactId) {
		this.artifactId = artifactId;
	}

	public Node getVersion() {
		return version;
	}

	public void setVersion(Node version) {
		this.version = version;
	}
	
}
