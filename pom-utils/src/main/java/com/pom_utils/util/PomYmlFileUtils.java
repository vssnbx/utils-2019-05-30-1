package com.pom_utils.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

public class PomYmlFileUtils {
	
	public static void writeList(String fullpath, List<String> list) throws Exception {
		File file1 = new File(fullpath);
		
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file1));
		for (int i=0; i<list.size(); i++) {
			bufferedWriter.write(list.get(i));
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}


	public static void readList(File fullpath, List<String> list) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(fullpath));
		String s;
		while ((s = bufferedReader.readLine()) != null) {
			list.add(s);
		}
		bufferedReader.close();
	}

}
