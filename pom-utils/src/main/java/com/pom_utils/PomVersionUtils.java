package com.pom_utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.pom_utils.util.PomUtils;
import com.pom_utils.util.PomYmlFileUtils;

public class PomVersionUtils {
	
	//private static final String ROOT_DIR_INPUT = "C:/opt/workspaces/workspace01/_levantamento/2018-06-12/microservices/";
	private static final String ROOT_DIR_INPUT = "C:/opt/workspaces/workspace01/_projects/2018-08-23_13-29-06_662165700/";
	
	private static final String NEW_VERSION = "999.999.999-SNAPSHOT";
	//private static final String NEW_VERSION = "LATEST";

	
	public static void main(String[] args) throws Exception {
		System.out.println("[INI - PomVersionUtils]");
		
		PomVersionUtils pomVersionUtils = new PomVersionUtils();
		pomVersionUtils.execute(ROOT_DIR_INPUT, NEW_VERSION);
		
		System.out.println("[FIM - PomVersionUtils]");
	}


	private void execute(String rootDirInput, String newVersion) throws Exception {
		File pomListFile = new File("c:/temp/pomList.txt");
		List<String> pomList = new ArrayList<>();
		PomYmlFileUtils.readList(pomListFile, pomList);
		
		PomUtils pomUtils = new PomUtils();
		pomUtils.processPomList(pomList, newVersion);
	}

}
