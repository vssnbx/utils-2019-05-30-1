package com.requests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

	public static List<String> getContent(File file) throws Throwable {
		List<String> list = new ArrayList<String>();
		BufferedReader rd = new BufferedReader(new FileReader(file));

		String line = "";
		while ((line = rd.readLine()) != null) {
			list.add(line);
		}
	
		rd.close();
		return list;
	}
	
	
}
