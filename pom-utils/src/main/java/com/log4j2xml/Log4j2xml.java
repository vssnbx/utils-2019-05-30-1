package com.log4j2xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.output.ByteArrayOutputStream;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class Log4j2xml {

	public static void main(String[] args) {
		
		File inputDir = new File("C:/vagner.da.silva/projects-gitlab/microservices");
		
		File[] listFiles = inputDir.listFiles();
		for (File projectDir : listFiles) {
			processListFiles(projectDir);
		}
	}	
	
	private static void processListFiles(File projectDir) {
		if (!projectDir.isDirectory()) {
			return;
		}
		
		File resourcesDir = new File(projectDir.getAbsolutePath() + "/src/main/resources");
		if (!resourcesDir.exists()) {
			log("Ignorando " + resourcesDir + " | Sem resources");
		}
		
		generateFileAndHandleExceptions(projectDir.getName(), resourcesDir);
	}

	private static void generateFileAndHandleExceptions(String projectName, File resourcesDir) {
		try {
			generateFile(projectName, resourcesDir);
//			log("Sucesso ao gerar arquivo do projeto:" + projectName);
			
		} catch (Exception e) {
			log("Exception ao gerar arquivo do projeto:" + projectName + " | " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void generateFile(String projectName, File resourcesDir) throws Exception {
		Configuration configuration = FreemarkerUtil.getConfigurationInstance();
		Template templateFile = configuration.getTemplate("log4j2.xml.ftl");		
		
		Map root = new HashMap<>(); 
		root.put("SERVICE_NAME_PLACEHOLDER", projectName);
		
//		Writer osw = new OutputStreamWriter(System.out);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Writer osw = new OutputStreamWriter(baos);
		
		templateFile.process(root, osw);
//		System.out.write(baos.toByteArray());
		
		File outputFile = new File(resourcesDir.getAbsolutePath() + "/log4j2.xml");
		if (outputFile.exists()) {
			return;
//			throw new RuntimeException(outputFile.getAbsolutePath() + " j� existe");
		}
		
		FileOutputStream fos = new FileOutputStream(outputFile);
		fos.write(baos.toByteArray());
		fos.close();
		
		baos.close();		
	}

	private static void log(String string) {
		System.out.println(string);
	}
}
