package com.log4j2xml;

import java.io.File;
import java.io.IOException;

import freemarker.core.TemplateConfiguration;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkerUtil {

	private static final String TEMPLATES_DIR="C:/vagner.da.silva/projects-gitlab_/templates"; 
	
	private static Configuration configuration;
	
	public static Configuration getConfigurationInstance() throws IOException {
		
		if (null == configuration) {
			Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
			cfg.setDirectoryForTemplateLoading(new File(TEMPLATES_DIR));
			cfg.setDefaultEncoding("UTF-8");
			cfg.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);
			cfg.setLogTemplateExceptions(false);
			cfg.setWrapUncheckedExceptions(true);
			
//			TemplateConfiguration templateConfiguration = new TemplateConfiguration();
			
			configuration = cfg;
		}

        return configuration;
	}
	
}
