package tests;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamTest {
	
	public static String trim(String s) {
		return
			s
	//		"  aa bb cc  "
			.replaceFirst("^[\\p{Blank}]+", "")
			.replaceAll("[\\p{Blank}]+$", "")
			;	
	}
	
	public static void main(String[] args) {
		//
		List<String[]> collectmap = Arrays.asList(null, "  aa  bb  cc  ", null)
			.stream()
				.map(a -> null != a && a.length() > 0 ? trim(a).split("[ ]+") : null)
				.filter(a -> null != a && a.length > 0)
				.collect(Collectors.toList());
		System.out.println(collectmap.get(1));
		//
		
		//
		List<String> list;
		
		list = Arrays.asList("B", "a");
		Comparator<String> c1 = new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				String to1 = Optional.ofNullable(o1).orElseThrow(RuntimeException::new);
				String to2 = Optional.ofNullable(o2).orElseThrow(RuntimeException::new);
//				String t01 = Optional.ofNullable(o1).orElseThrow(() -> new RuntimeException("o1 null"));
//				String t02 = Optional.ofNullable(o2).orElseThrow(() -> new RuntimeException("o2 null"));
				return to1.compareToIgnoreCase(to2);
			}
		};
		Collections.sort(list, c1);
		System.out.println("c1: "+list);

		list = Arrays.asList("B", "a");
		Comparator<String> c2 = (o1, o2) -> o1.compareToIgnoreCase(o2);
		Collections.sort(list, c2);		
		System.out.println("c2: "+list);
		
		list = Arrays.asList("B", "a");
		Comparator<String> c3 = String::compareToIgnoreCase;
		Collections.sort(list, c3);
		System.out.println("c3: "+list);	
		/**/
		list = Arrays.asList("B", "a");
		Comparator<String> c4 = StreamTest::compareToIgnoreCase;
		Collections.sort(list, c4);
		System.out.println("c4: "+list);			
		
		list = Arrays.asList("B", "a");
		Comparator<String> c5 = (o1, o2) -> StreamTest.compareToIgnoreCase(o1, o2);
		Collections.sort(list, c5);
		System.out.println("c5: "+list);
		
		//
		
		Map<String, Integer> map = new HashMap<>();
		map.put("a", 1);
		map.put("b", 2);
		
		Map<Entry<String, Integer>, Entry<String, Integer>> collect1 = map.entrySet().stream()
			.filter(a -> a.getKey().equals("b"))
			.collect(Collectors.toMap(a -> a, b -> b));
		System.out.println(collect1);
		
		Map<String, Integer> collect2 = map.entrySet().stream()
				.filter(a -> a.getKey().equals("b"))
				.collect(Collectors.toMap(a -> a.getKey(), b -> b.getValue()));
		System.out.println(collect2);			
		Map<String, Integer> collect3 = map.entrySet().stream()
				.filter(a -> a.getKey().equals("b"))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));		
		System.out.println(collect3);

		
	}

	public static int compareToIgnoreCase(String o1, String o2) {
		return o1.compareToIgnoreCase(o2);
	}	
	
}
