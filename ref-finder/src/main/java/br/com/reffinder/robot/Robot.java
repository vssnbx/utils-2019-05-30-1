package br.com.reffinder.robot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.reffinder.util.SubdirsFilesUtil;
import br.com.reffinder.util.Util;

public class Robot {
	
	private static final Logger logger = LoggerFactory.getLogger(Robot.class);
	
	public void run(String workspacePath, List<String> inputDataList, String outputFile) throws IOException {
		List<File> allFilesList = new SubdirsFilesUtil().getAllFiles(new File(workspacePath));
		
		for (String inputData : inputDataList) {
			searchStringInWorkSpace(allFilesList, inputData, outputFile);
		}
	}

	private void searchStringInWorkSpace(List<File> allFilesList, String stringToSearch, String outputFile) throws IOException {		
		logger.info("Searching for: " + stringToSearch);

		for (File file : allFilesList) {
			try {
				boolean found = searchStringInWorkSpace(file, stringToSearch, outputFile);
				if (found) {
					// para na primeira referência
					break;
				}
			} catch (Exception e) {
				logger.info("Exception no processamento:"+file.getAbsolutePath());
				e.printStackTrace();
			}
		}
	}

	private boolean searchStringInWorkSpace(File file, String stringToSearch, String outputFile) throws IOException {
		boolean found = false;
		BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()));
		int contLinha = 0;

		while (br.ready()) {
			contLinha++;
			String linha = br.readLine();
			
			if (linha.isEmpty() || linha == null) {					
				continue;
				
			} else if (
					(StringUtils.containsIgnoreCase(linha, stringToSearch)) 
//						&& (!linha.contains("package "))
//						&& (!linha.contains("import ")) 
					) {

				writeOutput(outputFile, stringToSearch, file, contLinha, linha);
				
				// para na primeira referência
				logger.info("Found: " + file.getAbsolutePath() + " stringToSearch:  " + stringToSearch +  " Line: " + contLinha);
				found = true;
				break;
			}
		}
		
		br.close();
		return found;
	}
	
	private void writeOutput(String outputFile, String stringToSearch, File file, int contLinha, String linha) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(outputFile), true));				

		bufferedWriter.write("[" + stringToSearch + "]");
		bufferedWriter.write("#");
		bufferedWriter.write("Line: " + Util.getFormatedLineNumber(contLinha));
		bufferedWriter.write("#");
		bufferedWriter.write("FileName: " + file.getPath());
		bufferedWriter.write("#");
		bufferedWriter.write(linha);
		bufferedWriter.newLine();

		bufferedWriter.close();
	}

}