package br.com.reffinder.main;

import java.util.ArrayList;
import java.util.List;

public class Config {

	public static final String WORKSPACEPATH = "C:/vagner.da.silva/projects-gitlab/";
	
	public static final String INPUTFILE = "C:\\opt\\workspaces\\workspace01\\_projects\\other\\Utils\\ref-finder\\FILES\\INPUT\\" + 
			"input";
	
	public static final String OUTPUTFILE = "C:\\opt\\workspaces\\workspace01\\_projects\\other\\Utils\\ref-finder\\FILES\\OUTPUT\\" + 
			"outPutFile";
	
	public static List<String> getProjectList() {
		List<String> projectList = new ArrayList<>();
		
		projectList.add("bpo/analise-doc-financeira/");
		projectList.add("bpo/analise-doc-financeira/");
		projectList.add("bpo/analise-doc-financeira/");
		projectList.add("bpo/getdocument-service/");
		projectList.add("bpo/taskAutomationDetran/");
		
		projectList.add("client/aftersale-client/");
		projectList.add("client/asynchronous-status-client/");
		projectList.add("client/base.exception-client/");
		projectList.add("client/clearsale-client/");
		projectList.add("client/client-santander/");
		projectList.add("client/consistency.model-client/");
		projectList.add("client/emailage-client/");
		projectList.add("client/emailsender-client/");
		projectList.add("client/gitlab-client/");
		projectList.add("client/gravame-client/");
		projectList.add("client/kafka-client/");
		projectList.add("client/legacy-client/");
		projectList.add("client/notifications-client/");
		projectList.add("client/occurence-client/");
		projectList.add("client/phase-client/");
		projectList.add("client/redis-client/");
		projectList.add("client/rpc-client/");
		projectList.add("client/token-client/");
		projectList.add("client/user-client/");
		
		projectList.add("component/capture-utils/");
		projectList.add("component/encryptor-utils/");
		projectList.add("component/jwt-authorize/");
		projectList.add("component/jwt-component/");
		projectList.add("component/profile-component/");
		projectList.add("component/spreadsheet-utils/");
		
		projectList.add("infraestrutura_vivere/financeira-cloud-config/");
		
		projectList.add("infrastructure/api-gateway/");
		projectList.add("infrastructure/financeira-cloud/");
		projectList.add("infrastructure/financeira-cloud-config/");
		projectList.add("infrastructure/financeira-dependencies.client/");
		projectList.add("infrastructure/financeira-dependencies.data/");
		projectList.add("infrastructure/financeira-dependencies.log4j2/");
		projectList.add("infrastructure/financeira-dependencies.logback/");
		projectList.add("infrastructure/financeira-dependencies.service/");
		projectList.add("infrastructure/financeira-dependencies/");
		projectList.add("infrastructure/infrastructure/");
		projectList.add("infrastructure/shinken-config/");
		
		projectList.add("legacy/vivere-app.cdc.batch/");
		projectList.add("legacy/vivere-app.cdc.cobol/");
		projectList.add("legacy/vivere-app.cdc.db/");
		projectList.add("legacy/vivere-app.cdc.estilo/");
		projectList.add("legacy/vivere-app.cdc/");
		projectList.add("legacy/vivere-cdc/");
		projectList.add("legacy/vivere-documento/");
		projectList.add("legacy/vivere-eai.cdc/");
		projectList.add("legacy/vivere-ws.core/");
		
		projectList.add("microservices/address-service/");
		projectList.add("microservices/aftersale-service/");
		projectList.add("microservices/assignment-consumer/");
		projectList.add("microservices/asynchronous-status-service/");
		projectList.add("microservices/atx.assets.cdc-service/");
		projectList.add("microservices/atx-service/");
		projectList.add("microservices/auth-service/");
		projectList.add("microservices/billet-service/");
		projectList.add("microservices/biometric-service/");
		projectList.add("microservices/bordero-service/");
		projectList.add("microservices/cancel-service/");
		projectList.add("microservices/capture.aftersale-service/");
		projectList.add("microservices/capture.assets.cdc-service/");
		projectList.add("microservices/capture.assets-service/");
		projectList.add("microservices/capture.vehicle-service/");
		projectList.add("microservices/certified.agents-service/");
		projectList.add("microservices/change.payment-service/");
		projectList.add("microservices/clearsale-service/");
		projectList.add("microservices/compose.attachment-consumer/");
		projectList.add("microservices/conciliation-service/");
		projectList.add("microservices/contract-consumer/");
		projectList.add("microservices/contract-service/");
		projectList.add("microservices/customer.ib-service/");
		projectList.add("microservices/customer-service/");
		projectList.add("microservices/dados-comp-tab-service/");
		projectList.add("microservices/denatran-service/");
		projectList.add("microservices/document.anticheat-service/");
		projectList.add("microservices/domains-service-jpa/");
		projectList.add("microservices/ek9-service/");
		projectList.add("microservices/emailage-service/");
		projectList.add("microservices/emailsender-service/");
		projectList.add("microservices/financeira-importfile.service/");
		projectList.add("microservices/formalization.aftersale-service/");
		projectList.add("microservices/formalization-service/");
		projectList.add("microservices/gravame-consumer/");
		projectList.add("microservices/gravame-service/");
		projectList.add("microservices/identification.assets.cdc-service/");
		projectList.add("microservices/identification.assets-service/");
		projectList.add("microservices/identification.vehicle-service/");
		projectList.add("microservices/identification-service/");
		projectList.add("microservices/importfile-service/");
		projectList.add("microservices/installment-service/");
		projectList.add("microservices/internetbanking-service/");
		projectList.add("microservices/kafka-producer-service/");
		projectList.add("microservices/konecta.consumer-service/");
		projectList.add("microservices/konecta.producer-service/");
		projectList.add("microservices/login-service/");
		projectList.add("microservices/minuta/");
		projectList.add("microservices/notifications.aftersale-service/");
		projectList.add("microservices/occurence.ccw-recorder/");
		projectList.add("microservices/panel-service/");
		projectList.add("microservices/payment-service/");
		projectList.add("microservices/phase-service/");
		projectList.add("microservices/pipefy-service/");
		projectList.add("microservices/profile-service/");
		projectList.add("microservices/proposal-refresher-service/");
		projectList.add("microservices/proposal-service/");
		projectList.add("microservices/scheduler-consumer/");
		projectList.add("microservices/scheduler-service/");
		projectList.add("microservices/signature-service/");
		projectList.add("microservices/simulation.assets.cdc-service/");
		projectList.add("microservices/simulation.assets-service/");
		projectList.add("microservices/simulation.vehicle-service/");
		projectList.add("microservices/simulation-consumer-service/");
		projectList.add("microservices/simulation-data/");
		projectList.add("microservices/simulation-service/");
		projectList.add("microservices/sms.token-service/");
		projectList.add("microservices/sms-consumer/");
		projectList.add("microservices/solicitation-consumer/");
		projectList.add("microservices/solicitation-service/");
		projectList.add("microservices/status-service/");
		projectList.add("microservices/tab-consumer/");
		projectList.add("microservices/tab-service/");
		projectList.add("microservices/token-service/");
		projectList.add("microservices/user-service/");
		projectList.add("microservices/vivere-token.consumer/");
		projectList.add("microservices/vivere-token/");
		projectList.add("microservices/vivere-ws.ticket/");
		projectList.add("microservices/vivere-ws.validar-conta/");
		
		return projectList;
	}	
}
