package br.com.reffinder.main;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.reffinder.robot.Robot;
import br.com.reffinder.util.Util;

public class RefFinderApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(RefFinderApplication.class);

	public static void main(String[] args) {
		try {
			String index = args[0];
			logger.info("INI - Processando:["+index+"]");
			
			List<String> inputDataList = Util.getInputData(Config.INPUTFILE + "-"+index+".txt");
			List<String> projectList = Config.getProjectList();
			
			Robot robot = new Robot();
			for (int i = 0; i < projectList.size(); i++) {
				String project = projectList.get(i);
				logger.info("Iniciando Processamento de Projeto [" + (i+1) + " / " + projectList.size() + "][" +project+ "]");
				robot.run(Config.WORKSPACEPATH + project, inputDataList, Config.OUTPUTFILE + "-"+index+".txt");
			}
			
			logger.info("FIM - Finalizando:["+index+"]");
			
		} catch (IOException e) {
			logger.info("FIM - Exception:["+e.getMessage()+"]");
			e.printStackTrace();
//			throw new RuntimeException("Houve um erro no processamento do robot" + e);
		}
	}
	

	
}
