package br.com.reffinder.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Util {

	public static List<String> getInputData(String inputdatafile) throws IOException {
		List<String> inputData = new ArrayList<String>();
		File inputDataFile = new File(inputdatafile);

		BufferedReader br = new BufferedReader(new FileReader(inputDataFile.getAbsolutePath()));

		while (br.ready()) {
			String linha = br.readLine();

			if (linha.isEmpty() || linha == null) {
				continue;
			} else {
				inputData.add(linha.toLowerCase().trim()); 
			}
		}

		br.close();
		
		return inputData;
	}

	public static String getFormatedLineNumber(int contLinha) {

		String cont = String.valueOf(contLinha);
		int length = 4 - cont.length();

		if (length != 0) {
			for (int i = 0; i < length; i++) {
				cont = "0" + cont;
			}
		}

		return cont;
	}
}