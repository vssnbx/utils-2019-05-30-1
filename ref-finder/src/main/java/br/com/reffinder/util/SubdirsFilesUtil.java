package br.com.reffinder.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SubdirsFilesUtil {

	private static final Logger logger = LoggerFactory.getLogger(SubdirsFilesUtil.class);
	
	private List<File> allDirectoriesTemp = new ArrayList<File>();
	
	public List<File> getAllFiles(File dir) {
		List<File> allFilesList = new ArrayList<File>();

		// obtendo todos os diretórios
		List<File> allDirectories = getAllDirectories(dir);
		for (File directory : allDirectories) {
			
			// percorrendo todos os arquivos de cada diretório
			for (File file : directory.listFiles()) {
				if (file.isDirectory()) {
					logger.info("Ignorando Dir [busca de arquivos]: " + file.getAbsolutePath());
					continue;
				}
				
				boolean added = false;
				
				// percorrendo a lista de extensões a  considerar
				for (String extension : correctFileExtensions()) {
					
					// checando se o arquivo contém a extensão desejada
					if (file.getAbsolutePath().contains(extension)) {
						allFilesList.add(file);
						added = true;
						break;
					}

				}

				if (!added) {
					logger.info("Ignorando File [extensão desconsiderada]: " + file.getAbsolutePath());
				}
			}
		}

		return allFilesList;
	}

	private List<File> getAllDirectories(File directory) {
		this.allDirectoriesTemp.clear();
		
		listDirectories(directory);

		return this.allDirectoriesTemp;
	}

	private void listDirectories(File directory) {		
		
		// desconsiderando pastas de configuração
		if (directory.isDirectory() 
				&& !directory.getAbsolutePath().contains(".metadata")
				&& !directory.getAbsolutePath().contains(".recommenders")
				&& !directory.getAbsolutePath().contains(".settings")
				&& !directory.getAbsolutePath().contains(".git") 
				&& !directory.getAbsolutePath().contains("META-INF")
				&& !directory.getAbsolutePath().contains("target")
				&& !directory.getAbsolutePath().contains("vivere-app.cdc.db")
				&& !directory.getAbsolutePath().contains("\\src\\test\\")) {
		
			allDirectoriesTemp.add(directory);
			
			String[] subDirectory = directory.list();

			if (subDirectory != null) {
				for (String dir : subDirectory) {
					listDirectories(new File(directory + File.separator + dir));
				}
			}
		}
	}
	
	private List<String> correctFileExtensions() {
		List<String> correctFileExtensions = new ArrayList<String>();

		correctFileExtensions.add(".java");
		correctFileExtensions.add(".xml");
		correctFileExtensions.add(".yml");
		correctFileExtensions.add(".html");
		correctFileExtensions.add(".css");
		correctFileExtensions.add(".sql");
		correctFileExtensions.add(".xsd");
		correctFileExtensions.add(".js");
		correctFileExtensions.add(".vm");
		correctFileExtensions.add(".pco");
		correctFileExtensions.add(".properties");
		//correctFileExtensions.add(".jvm");

		return correctFileExtensions;
	}
	
}
